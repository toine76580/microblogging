from django.apps import AppConfig


class BlogReviewConfig(AppConfig):
    name = 'blog_review'
