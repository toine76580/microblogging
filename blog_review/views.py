from django.shortcuts import render
from blog_review.models import Microblogging

def index(request):
    blog = Microblogging.objects.all()
    return render(request, 'index.html', {'latests_blog': blog})

#def blog(request, slug):
 #   blog = get_object_or_404(Blog, slug=slug)
  #  return render(request, 'blog.html', {'blog': blog})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
 

