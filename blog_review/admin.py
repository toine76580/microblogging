from django.contrib import admin
from blog_review.models import *


admin.site.register(Microblogging)



class BlogAdmin(admin.ModelAdmin):
    list_display = ('name', 'release_date', )
    search_fields = ('name', )
    list_filter = ('types', )

