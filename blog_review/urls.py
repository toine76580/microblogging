from django.urls import path
from blog_review import views
app_name = 'blog_review'



urlpatterns = [
    path('', views.index, name='index'),
   #path('blog/<slug:slug>/', views.blog, name='blog'),
   #path('login',  views.login, name='login'),
   #path('logout', views.logout, name='logout'),
    path('signup', views.signup, name='signup'),
]