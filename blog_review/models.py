from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


#class User(models.Model):
  #  Login = models.CharField(max_length=20)
  #  Password = models.CharField(max_length=20)
  #  
  #  class Meta:
   #     verbose_name = 'User'
   #     verbose_name_plural = 'User'
class Microblogging(models.Model):
  
    Picture = models.ImageField()
    Title = models.CharField(max_length=40)
    Author = models.ForeignKey(User, on_delete=models.CASCADE)
    Date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Blog'
        verbose_name_plural = 'Blog'